import os
import xlrd
from datetime import datetime
import dbf
from shutil import copyfile
import calendar
from settings import kv_acc


directory = "./xls/"
files = os.listdir(directory)

date = datetime.now()
last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
ex_file_name = 'SHOR039_' + last_day.strftime('%d%m%y') + '.dbf'


PLACEPAY = 'реестры'


def calc_massive():
    total_sum = 0
    ent_dict = []
    for f in files:
        rd = xlrd.open_workbook(directory + f, formatting_info=True)
        sheet = rd.sheet_by_index(0)
        for rowNum in range(sheet.nrows):
            row = sheet.row_values(rowNum)
            if type(row[1]) != str:
                row.pop(0)
                row.pop(0)
                row[0] = datetime(*xlrd.xldate_as_tuple(row[0], rd.datemode))
                row[0] = datetime.strftime(row[0], '%d.%m.%Y')
                total_sum += row[2]
                for r in kv_acc:
                    if row[1] == r[1]:
                        row[1] = r[0]
                ent_dict.append(row)
    return ent_dict, total_sum


def make_dbf(rows_massive, prefix_name):
    copyfile('SHOR039_YMMDD.dbf', ex_file_name)
    table = dbf.Table(ex_file_name, codepage='cp866')
    table.open(mode=dbf.READ_WRITE)
    for r in rows_massive:
        table.append(
            (datetime.strptime(r[0], '%d.%m.%Y'), r[1], r[2], PLACEPAY, prefix_name))


def main():
    prefix = input('Жку(1) или кап ремонт(2)?')
    if prefix == '1':
        prefix = 'SHOR039'
    else:
        prefix = 'SHOR239'

    rows, total = calc_massive()
    make_dbf(rows, prefix)
    print('Done, imported rows = {}, total sum = {}'.format(len(rows), total))


if __name__ == '__main__':
    main()
